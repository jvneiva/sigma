﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Agenda
{
   public class FichaAnimalDTO
    {
        public int Id { get; set; }
        public string DataProxVc { get; set; }
        public string NomeProxVc { get; set; }
    }
}
