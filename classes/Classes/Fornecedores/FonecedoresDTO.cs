﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Cliente
{
   public class FornecedoresDTO
    {
        public int Id { get; set; }
        public int IdEstado { get; set; }
        public string Empresa { get; set; }
        public string NmFantasia { get; set; }
        public int Cnpj { get; set; }
        public string Endereco { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public int Cpf { get; set; }
        public int Telefone { get; set; }
        public string Email { get; set; }
        public string Local { get; set; }
    }
}
